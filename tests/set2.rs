extern crate rand;

use cryptopals::*;
use std::fs;
use rand::Rng;

#[test]
fn challenge9() {
    assert_eq!(aes::pad_pkcs7(b"YELLOW SUBMARINE".to_vec(), 20).len(), 20);
}

#[test]
fn challenge10() {
    let contents = fs::read_to_string("resources/2-10.txt").unwrap();
    let bytes = converts::base64_to_bytes(contents);
    let key: Vec<u8> = "YELLOW SUBMARINE".bytes().collect();
    let iv = vec![0; 16];
    let text = aes::decrypt_cbc(&bytes, &key, &iv);

    assert!(
        converts::bytes_to_ascii(text).contains("Well that's my DJ Deshay cuttin' all them Z's")
    );
}

#[test]
#[ignore]
fn challenge11() {
    let contents = fs::read_to_string("resources/2-11-mine.txt").unwrap();
    let bytes: Vec<u8> = contents.bytes().collect();

    for trial in 0..10 {
        let (ct, scheme) = cryptobreak::encryption_oracle(bytes.clone());
        let guess_scheme = cryptobreak::guess_aes_moo(ct);

        assert_eq!(guess_scheme, scheme, "Failed at trial {:02}", trial + 1);
    }
}

#[test]
#[ignore]
fn challenge12() {
    // Read the text that should be append to create the oracle
    let contents = fs::read_to_string("resources/2-12-append.txt").unwrap();
    let bytes = converts::base64_to_bytes(contents);
    let key = cryptobreak::random_bytes(aes::BLOCK_SIZE);
    let oracle = cryptobreak::make_oracle(0, bytes.clone(), key);
    let block_size = cryptobreak::get_block_size(&oracle);

    assert_eq!(block_size, aes::BLOCK_SIZE);

    assert_eq!(
        cryptobreak::guess_aes_moo(oracle(vec![0; 2 * block_size])),
        aes::MODE::ECB
    );

    let text = cryptobreak::get_oracle_plaintext(oracle, block_size);

    assert_eq!(aes::unpad_pkcs7(text), bytes);
}

#[test]
#[ignore]
fn challenge13() {
    let email = "xxxBIGGAMERxxx789789@gmail.com";
    let key = cryptobreak::random_bytes(aes::BLOCK_SIZE);
    let profile = cookies::profile_for(email).bytes().collect();
    let oracle = cryptobreak::make_oracle(0, profile, key);
    let block_size = cryptobreak::get_block_size(&oracle);
    let text = cryptobreak::get_oracle_plaintext(oracle, block_size);

    assert_eq!(
        String::from_utf8(aes::unpad_pkcs7(text)).unwrap(),
        format!("email={}&uid=10&role=user", email)
    );
}

/// Return the tuple containing the ciphertext, key, and iv.
fn c16_enc(payload: &str) -> (Vec<u8>, Vec<u8>, Vec<u8>) {
    let payload = payload.replace("=", "?");
    let payload = payload.replace(";", "?");
    let mut s = "comment1=cooking%20MCs;userdata=".to_string();
    s.push_str(payload.as_str());
    s.push_str(";comment2=%20like%20a%20pound%20of%20bacon");

    let pt = aes::pad_pkcs7(converts::ascii_to_bytes(s.as_str()), 16);
    let mut key = [0u8; 16];
    let mut iv = [0u8; 16];
    rand::thread_rng().fill(&mut key);
    rand::thread_rng().fill(&mut iv);
    let key = key.to_vec();
    let iv = iv.to_vec();

    (aes::encrypt_cbc(&pt, &key, &iv), key, iv)
}

/// Return true if the ciphertext contains ";admin=true;", false otherwise.
/// We don't need to unpad the decrypted string because we are only searching through the string.
fn c16_check(ct: &Vec<u8>, key: &Vec<u8>, iv: &Vec<u8>) -> bool {
    let pt = converts::bytes_to_ascii(aes::decrypt_cbc(ct, key, iv));
    println!("{}", &pt);

    pt.contains(";admin=true;")
}

#[test]
fn c16_skelly() {
    let (ct, key, iv) = c16_enc(";admin=true");
    assert!(!c16_check(&ct, &key, &iv));
}

#[test]
fn challenge16() {
    let (mut ct, key, iv) = c16_enc(";admin=true");
    ct[16] ^= 4;        // '?' ^ ';'
    ct[22] ^= 2;        // '?' ^ '='
    assert!(c16_check(&ct, &key, &iv));
}
