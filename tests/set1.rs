use cryptopals::*;
use std::fs;

#[test]
fn challenge1() {
    let a = "49276d206b696c6c696e6720796f757220627261696e206c696b65206120706f69736f6e6f7573206d757368726f6f6d";
    let b = "SSdtIGtpbGxpbmcgeW91ciBicmFpbiBsaWtlIGEgcG9pc29ub3VzIG11c2hyb29t";
    assert_eq!(converts::hex_to_base64(a), b);
}

#[test]
fn challenge2() {
    let a = "1c0111001f010100061a024b53535009181c";
    let b = "686974207468652062756c6c277320657965";
    let c = "746865206b696420646f6e277420706c6179";
    assert_eq!(ops::xor_strs(a, b), c);
}

#[test]
fn challenge3() {
    let hex = converts::str_to_bytes(
        "1b37373331363f78151b7f2b783431333d78397828372d363c78373e783a393b3736",
    );
    let key = cryptobreak::dec_hex_rep1(hex.clone());
    assert_eq!(
        converts::bytes_to_ascii(cryptobreak::rep_key_xor(hex, vec![key])),
        "Cooking MC's like a pound of bacon"
    );
}

#[test]
fn challenge4() {
    fn score(s: String) -> u8 {
        let hex = converts::str_to_bytes(s.as_str());
        let key = cryptobreak::dec_hex_rep1(hex.clone());
        let dec = cryptobreak::rep_key_xor(hex, vec![key]);

        dec.iter().fold(0, |s, x| {
            if x.is_ascii_alphanumeric() || x == &b' ' {
                s + 1
            } else if !x.is_ascii() {
                if s == 0 {
                    s
                } else {
                    s - 1
                }
            } else {
                s
            }
        })
    }

    let content = fs::read_to_string("resources/1-4.txt").unwrap();
    let mut best_score = 0;
    let mut best_line = String::new();

    for line in content.lines() {
        let test_score = score(line.to_string());
        if test_score > best_score {
            best_score = test_score;
            best_line = line.to_string();
        }
    }

    let hex = converts::str_to_bytes(best_line.as_str());
    let key = cryptobreak::dec_hex_rep1(hex.clone());
    let dec = cryptobreak::rep_key_xor(hex, vec![key]);

    assert_eq!(
        converts::bytes_to_ascii(dec),
        "Now that the party is jumping\n"
    );
}

#[test]
fn challenge5() {
    let s = "Burning 'em, if you ain't quick and nimble\nI go crazy when I hear a cymbal".bytes();
    let k = "ICE".bytes();
    let e = cryptobreak::rep_key_xor(s, k);
    let expect = "0b3637272a2b2e63622c2e69692a23693a2a3c6324202d623d63343c2a26226324272765272a282b2f20430a652e2c652a3124333a653e2b2027630c692b20283165286326302e27282f";
    let expect = converts::str_to_bytes(expect);

    assert_eq!(e, expect);
}

#[test]
fn challenge6() {
    // Score keysize by averaging all possible blocks of repeating keys
    fn score_keysize(ks: u32, v: &[u8]) -> u32 {
        let mut dist = 0;
        let ks = ks as usize;
        let num_chunks = v.len() / ks;
        for i in 0..num_chunks {
            // Weighted average of 1 pair (distance per byte)
            dist += ops::hamming_slice(&v[i..(i + ks)], &v[(i + ks)..(i + 2 * ks)]) / ks as u32;
        }

        // Weighted average of all pairs (distance per byte per pair)
        dist * 100 / num_chunks as u32
    }

    let contents = fs::read_to_string("resources/1-6.txt").unwrap();

    // Obtain the key size
    let bytes = converts::base64_to_bytes(contents);
    let keysize = (2..40).min_by_key(|&ks| score_keysize(ks, &bytes)).unwrap();

    // Obtain the key itself
    let mut m_bytes = bytes.clone().into_iter();
    let mut key = Vec::new();
    for _ in 0..keysize {
        let block = m_bytes.clone().step_by(keysize as usize);
        key.push(cryptobreak::dec_hex_rep1(block));

        m_bytes.next();
    }

    // The key!
    assert_eq!(
        String::from_utf8(key).unwrap(),
        "Terminator X: Bring the noise"
    );
}

#[test]
fn challenge7() {
    let contents = fs::read_to_string("resources/1-7.txt").unwrap();
    let bytes = converts::base64_to_bytes(contents);
    let key = b"YELLOW SUBMARINE".to_vec();
    let text = String::from_utf8(aes::decrypt_ecb(&bytes, &key)).unwrap();

    assert!(text.contains("Play that funky music Come on, Come on, let me hear"));
}

#[test]
fn challenge8() {
    // Find the number of duplicate blocks
    fn score(line: String, block_size: usize) -> usize {
        let bytes = line.bytes().collect::<Vec<u8>>();
        cryptobreak::count_repeating_blocks(bytes, block_size)
    }

    let content = fs::read_to_string("resources/1-8.txt").unwrap();
    let ecb_encrypted = content
        .lines()
        .max_by_key(|l| score(l.to_string(), 16))
        .unwrap();

    assert!(ecb_encrypted.contains("d8806197"));
}
