extern crate rand;

use cryptopals::*;
use rand::Rng;

static CHALL17_STRINGS: &'static [&'static str] = &[
    "MDAwMDAwTm93IHRoYXQgdGhlIHBhcnR5IGlzIGp1bXBpbmc=",
    "MDAwMDAxV2l0aCB0aGUgYmFzcyBraWNrZWQgaW4gYW5kIHRoZSBWZWdhJ3MgYXJlIHB1bXBpbic=",
    "MDAwMDAyUXVpY2sgdG8gdGhlIHBvaW50LCB0byB0aGUgcG9pbnQsIG5vIGZha2luZw==",
    "MDAwMDAzQ29va2luZyBNQydzIGxpa2UgYSBwb3VuZCBvZiBiYWNvbg==",
    "MDAwMDA0QnVybmluZyAnZW0sIGlmIHlvdSBhaW4ndCBxdWljayBhbmQgbmltYmxl",
    "MDAwMDA1SSBnbyBjcmF6eSB3aGVuIEkgaGVhciBhIGN5bWJhbA==",
    "MDAwMDA2QW5kIGEgaGlnaCBoYXQgd2l0aCBhIHNvdXBlZCB1cCB0ZW1wbw==",
    "MDAwMDA3SSdtIG9uIGEgcm9sbCwgaXQncyB0aW1lIHRvIGdvIHNvbG8=",
    "MDAwMDA4b2xsaW4nIGluIG15IGZpdmUgcG9pbnQgb2g=",
    "MDAwMDA5aXRoIG15IHJhZy10b3AgZG93biBzbyBteSBoYWlyIGNhbiBibG93",
];

fn challenge17_generator() -> (Vec<u8>, Vec<u8>, Vec<u8>) {
    let sampled = aes::pad_pkcs7(converts::base64_to_bytes(
            String::from(CHALL17_STRINGS[rand::thread_rng().gen_range(0, CHALL17_STRINGS.len())])
    ), 16);
    let mut key = [0u8; 16];
    let mut iv = [0u8; 16];
    rand::thread_rng().fill(&mut key);
    rand::thread_rng().fill(&mut iv);
    let key = key.to_vec();
    let iv = iv.to_vec();
    println!("{:?}", sampled);
    let ciphertext = aes::encrypt_cbc(&sampled, &key, &iv);

    (ciphertext, key, iv)
}

fn challenge17_oracle(ct: &Vec<u8>, key: &Vec<u8>, iv: &Vec<u8>) -> bool {
    let decrypted = aes::decrypt_cbc(ct, key, iv);
    println!("{:?}", &decrypted);
    aes::is_valid_pkcs7(&decrypted, 16)
}

#[test]
fn c17_identity() {
    let (c, k, i) = challenge17_generator();
    assert!(challenge17_oracle(&c, &k, &i));
}

#[test]
fn challenge17() {
}
