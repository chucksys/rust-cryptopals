//! A collection of functions that operate on certain values.

use super::converts::bytes_to_str;
use super::converts::str_to_bytes;

/// XOR two sequences of bytes together
pub fn xor_bytes(a: impl IntoIterator<Item = u8>, b: impl IntoIterator<Item = u8>) -> Vec<u8> {
    a.into_iter().zip(b).map(|(x, y)| x ^ y).collect()
}

/// XOR two hex strings together
pub fn xor_strs(a: &str, b: &str) -> String {
    let a = str_to_bytes(a);
    let b = str_to_bytes(b);

    bytes_to_str(xor_bytes(a, b))
}

/// Find the hamming distance of two bytes
pub fn hamming_byte(a: u8, b: u8) -> u32 {
    (a ^ b).count_ones()
}

/// Find hamming distance of two slices of bytes
pub fn hamming_slice(a: &[u8], b: &[u8]) -> u32 {
    hamming(a.to_vec(), b.to_vec())
}

/// Find the hamming distance between a sequence of bytes
pub fn hamming(a: impl IntoIterator<Item = u8>, b: impl IntoIterator<Item = u8>) -> u32 {
    a.into_iter().zip(b).map(|(a, b)| hamming_byte(a, b)).sum()
}

#[cfg(test)]
mod tests {
    use super::*;

    mod test_xor_strs {
        use super::xor_strs;

        #[test]
        fn basic_stuff() {
            assert_eq!(xor_strs("0000", "1213"), "1213");
            assert_eq!(xor_strs("0000", "0000"), "0000");
            assert_eq!(xor_strs("0112", "0112"), "0000");
        }
    }

    mod test_xor_bytes {
        use super::xor_bytes;

        #[test]
        fn basic_stuff() {
            assert_eq!(xor_bytes(vec![0, 0], vec![12, 13]), vec![12, 13]);
            assert_eq!(xor_bytes(vec![0, 0], vec![0, 0]), vec![0, 0]);
            assert_eq!(xor_bytes(vec![1, 12], vec![1, 12]), vec![0, 0]);
        }
    }

    mod test_hamming_byte {
        use super::hamming_byte;

        #[test]
        fn basic_stuff() {
            assert_eq!(hamming_byte(0b01, 0b00), 1);
            assert_eq!(hamming_byte(0b11, 0b00), 2);
            assert_eq!(hamming_byte(0b01, 0b01), 0);
            assert_eq!(hamming_byte(0b11001, 0b01100), 3);
        }
    }

    mod test_hamming {
        use super::hamming;

        #[test]
        fn example() {
            let a = "this is a test".bytes();
            let b = "wokka wokka!!!".bytes();

            assert_eq!(hamming(a, b), 37);
        }

        #[test]
        fn basic_stuff() {
            assert_eq!(
                hamming(vec![0b000, 0b001, 0b111], vec![0b111, 0b001, 0b000]),
                6
            );
        }
    }
}
