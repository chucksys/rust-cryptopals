//! A collection of functions that help in converting one thing to another.

/// Convert a traditional byte into a number by hex.
///
/// # Examples
///
/// ```
/// assert_eq!(cryptopals::converts::char_to_hex(b'a'), 0xa);
/// ```
///
/// # Panics
///
/// The byte must be a valid hex digit (either uppercase or lowercase - it doesn't matter). It
/// panics if it is given an invalid byte (not matching regular expression `[0-9A-Fa-f]`).
pub fn char_to_hex(c: u8) -> u8 {
    match c {
        48..=57 => c - 48,  // 0 - 9
        65..=70 => c - 55,  // A - F
        97..=102 => c - 87, // a - f
        _ => panic!("Invalid character '{}'; cannot be converted to hex", c),
    }
}

/// Convert a hex string into a sequence of bytes.
///
/// # Examples
///
/// ```
/// let s = "120a0b0c";
///
/// assert_eq!(cryptopals::converts::str_to_bytes(s), vec![0x12, 0x0a, 0x0b, 0x0c]);
/// ```
///
/// # Panics
///
/// The hex string must have a size that is a multiple of 2, and all characters must be valid hex
/// digits (hex digits can be uppercase or lowercase).
pub fn str_to_bytes(s: &str) -> Vec<u8> {
    assert!(s.len() % 2 == 0);

    let mut v = Vec::new();
    let mut temp: u8 = 0;

    for (i, b) in s.bytes().enumerate() {
        if i % 2 == 0 {
            temp = char_to_hex(b);
        } else {
            v.push((temp << 4) + char_to_hex(b));
            temp = 0;
        }
    }

    v
}

/// Convert an ascii string into a sequence of bytes.
///
/// # Examples
///
/// ```
/// use cryptopals::converts::ascii_to_bytes;
///
/// assert_eq!(ascii_to_bytes("012345"), vec![48, 49, 50, 51, 52, 53]);
/// assert_eq!(ascii_to_bytes("abc"), vec![97, 98, 99]);
/// ```
pub fn ascii_to_bytes(s: &str) -> Vec<u8> {
    String::from(s).into_bytes()
}

/// Convert a sequence of bytes into a hex string.
///
/// # Examples
///
/// ```
/// let v = vec![0x12, 0x08, 0x72];
///
/// assert_eq!(cryptopals::converts::bytes_to_str(v), "120872");
/// ```
pub fn bytes_to_str(v: impl IntoIterator<Item = u8>) -> String {
    v.into_iter()
        .fold(String::new(), |acc, x| format!("{}{:02x}", acc, x))
}

/// Convert a sequence of bytes into an ascii string.
pub fn bytes_to_ascii(v: impl IntoIterator<Item = u8>) -> String {
    v.into_iter().fold(String::new(), |s, b| {
        if b.is_ascii() {
            format!("{}{}", s, b as char)
        } else {
            format!("{}\\{:02x}", s, b)
        }
    })
}

/// Convert a sequence of bytes into base64 6 bit format.
///
/// # Examples
///
/// ```
/// // Numbers               0            2            4
/// let seq =  vec![0b0000_0000, 0b0000_0010, 0b0000_0100];
/// let bits = vec![0b000_000, 0b000_000, 0b001_000, 0b000_100];
///
/// assert_eq!(cryptopals::converts::bytes_to_6bit(seq), bits);
/// ```
pub fn bytes_to_6bit(v: impl IntoIterator<Item = u8>) -> Vec<u8> {
    let v: Vec<u8> = v.into_iter().collect();
    let mut base64s = Vec::new();

    // Chunk by 3s for easier base64 pushing
    for threes in v.chunks(3) {
        match threes.len() {
            3 => {
                base64s.push(threes[0] >> 2);
                base64s.push(((threes[0] << 4) | (threes[1] >> 4)) & 0b111_111);
                base64s.push(((threes[1] << 2) | (threes[2] >> 6)) & 0b111_111);
                base64s.push(threes[2] & 0b111_111);
            }
            2 => {
                base64s.push(threes[0] >> 2);
                base64s.push(((threes[0] << 4) | (threes[1] >> 4)) & 0b111_111);
                base64s.push((threes[1] << 2) & 0b111_111);
                base64s.push(64);
            }
            1 => {
                base64s.push(threes[0] >> 2);
                base64s.push((threes[0] << 4) & 0b111_111);
                base64s.push(64);
                base64s.push(64);
            }
            l => panic!("Invalid length ({}). How did you manage to do that???", l),
        }
    }

    base64s
}

/// Convert a 6 bit number to a base64 character.
///
/// # Examples
///
/// ```
/// assert_eq!(cryptopals::converts::hexlet_to_base64(12), 'M');
/// ```
///
/// # Panics
///
/// The 6 bit number must be 6 bits big!
pub fn hexlet_to_base64(x: u8) -> char {
    match x {
        0..=25 => (x + 65) as char,
        26..=51 => (x + 71) as char,
        52..=61 => (x - 4) as char,
        62 => '+',
        63 => '/',
        64 => '=',
        _ => panic!("Character must be within [0, 63]!!!"),
    }
}

/// Convert a base64 character to digit.
pub fn base64_to_hexlet(x: u8) -> u8 {
    match x {
        b'A'..=b'Z' => x - 65,
        b'a'..=b'z' => x - 71,
        b'0'..=b'9' => x + 4,
        b'+' => 62,
        b'/' => 63,
        b'=' => 64,
        _ => 100, // Parse for errors
    }
}

/// Convert a base64 sequence into a sequence of bytes.
///
/// This function ignores all characters that are not part of the base64 character set. After
/// filtering out the irrelevant characters, the string is checked to see if it still adheres to
/// the "multiple of 4 in length" rule.
///
/// # Examples
///
/// ```
/// let base64 = "HUIfTQsPAh9PE048GmllH0kcDk4TAQsHThsBFkU2AB4BSWQgVB0dQzNTTmVS";
/// let hex = "1D421F4D0B0F021F4F134E3C1A69651F491C0E4E13010B074E1B01164536001E01496420541D1D4333534E6552";
///
/// assert_eq!(cryptopals::converts::base64_to_bytes(base64.to_string()),
///            cryptopals::converts::str_to_bytes(hex));
/// ```
///
/// # Panics
///
/// The string (ignoring irrelevant characters) must have a length that is a multiple of 4. The
/// equal signs must also be in the correct positions. However, since we only check per 4-byte
/// chunk, it is technically possible to have chunks that have equal signs that are in the middle
/// of the string.
pub fn base64_to_bytes(s: String) -> Vec<u8> {
    let prelim = s
        .bytes()
        .map(base64_to_hexlet)
        .filter(|&x| x != 100)
        .collect::<Vec<u8>>();
    let mut bytes = Vec::new();

    assert!(
        prelim.len() % 4 == 0,
        "Bytes length ({}) must be a multiple of 4",
        prelim.len()
    );

    // Chunks them in 4s, for easier byte pushing
    for fours in prelim.chunks(4) {
        assert_ne!(fours[0], 64, "= sign at the wrong position");
        assert_ne!(fours[1], 64, "= sign at the wrong position");

        if fours[2] == 64 && fours[3] == 64 {
            // Only 1 byte is written
            bytes.push((fours[0] << 2) | (fours[1] >> 4));
        } else if fours[3] == 64 {
            // 2 bytes are written
            bytes.push((fours[0] << 2) | (fours[1] >> 4));
            bytes.push((fours[1] << 4) | (fours[2] >> 2));
        } else if fours[2] != 64 && fours[3] != 64 {
            // All 3 bytes are written
            bytes.push((fours[0] << 2) | (fours[1] >> 4));
            bytes.push((fours[1] << 4) | (fours[2] >> 2));
            bytes.push((fours[2] << 6) | (fours[3]));
        } else {
            panic!("Invalid sequence of bytes {:?}", fours);
        }
    }

    bytes
}

/// Convert a sequence of base64 6 bit formats into base64 characters.
pub fn bits_to_base64(v: impl IntoIterator<Item = u8>) -> String {
    v.into_iter().map(hexlet_to_base64).collect()
}

/// Convert a hex string into base64 string
///
/// # Examples
///
/// ```
/// let hex = "ab54";
///
/// assert_eq!(cryptopals::converts::hex_to_base64(hex), "q1Q=");
/// ```
pub fn hex_to_base64(s: &str) -> String {
    bits_to_base64(bytes_to_6bit(str_to_bytes(s)))
}

#[cfg(test)]
mod tests {
    use super::*;

    mod test_base64_to_bytes {
        use super::base64_to_bytes;

        #[test]
        fn small() {
            assert_eq!(base64_to_bytes("TQ==".to_string()), vec![b'M']);
            assert_eq!(base64_to_bytes("TWE=".to_string()), vec![b'M', b'a']);
            assert_eq!(base64_to_bytes("TWFu".to_string()), vec![b'M', b'a', b'n']);
        }

        #[test]
        fn ignore_non_b64() {
            assert_eq!(
                base64_to_bytes("TQ\nTQTQ==\t\t   ".to_string()),
                base64_to_bytes("TQTQTQ==".to_string())
            );
        }
    }

    mod test_base64_to_hexlet {
        use super::base64_to_hexlet;

        #[test]
        fn sample() {
            assert_eq!(base64_to_hexlet(b'A'), 0);
            assert_eq!(base64_to_hexlet(b'2'), 54);
            assert_eq!(base64_to_hexlet(b'M'), 12);
            assert_eq!(base64_to_hexlet(b'/'), 63);
        }
    }

    mod test_hex_to_base64 {
        use super::hex_to_base64;

        #[test]
        fn small() {
            assert_eq!(hex_to_base64("04"), "BA==");
        }

        #[test]
        fn problem() {
            assert_eq!(hex_to_base64("12345678"), "EjRWeA==");
        }
    }

    mod test_hexlet_to_base64 {
        use super::hexlet_to_base64;

        #[test]
        fn sample() {
            assert_eq!(hexlet_to_base64(0), 'A');
            assert_eq!(hexlet_to_base64(54), '2');
            assert_eq!(hexlet_to_base64(12), 'M');
            assert_eq!(hexlet_to_base64(63), '/');
        }
    }

    mod test_char_to_hex {
        use super::char_to_hex;

        #[test]
        fn entire_domain() {
            "0123456789"
                .bytes()
                .enumerate()
                .for_each(|(i, b)| assert_eq!(char_to_hex(b), i as u8));
            "abcdef"
                .bytes()
                .enumerate()
                .for_each(|(i, b)| assert_eq!(char_to_hex(b), i as u8 + 10));
            "ABCDEF"
                .bytes()
                .enumerate()
                .for_each(|(i, b)| assert_eq!(char_to_hex(b), i as u8 + 10));
        }

        #[test]
        #[should_panic]
        fn out_of_domain() {
            char_to_hex(b'G');
        }
    }

    mod test_str_to_bytes {
        use super::str_to_bytes;

        #[test]
        fn small_sample() {
            assert_eq!(str_to_bytes("abcd"), vec![0xab, 0xcd]);
            assert_eq!(str_to_bytes("04"), vec![4]);
        }

        #[test]
        #[should_panic]
        fn bad_string_size() {
            str_to_bytes("abc");
        }

        #[test]
        #[should_panic]
        fn bad_string_chars() {
            str_to_bytes("abcg1235");
        }
    }

    mod test_bytes_to_6bit {
        use super::bytes_to_6bit;

        #[test]
        fn small_sample() {
            assert_eq!(
                bytes_to_6bit(vec![0b011111_11, 0b0101_0101, 0b11_100011]),
                vec![0b011111, 0b110101, 0b010111, 0b100011]
            );
            assert_eq!(
                bytes_to_6bit(vec![0b0000_0100]),
                vec![0b000_001, 0b000_000, 64, 64]
            );
        }
    }
}
