//! A library that tries to complete the cryptopals challenge, but in Rust.
pub mod aes;
pub mod converts;
pub mod cookies;
pub mod cryptobreak;
pub mod ops;
