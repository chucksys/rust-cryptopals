use std::collections::HashMap;

/// Parse a cookie into a HashMap.
///
/// # Examples
///
/// A structured cookie `username=bob&password=xxx_BOB_xxx` should be interpreted as a HashMap.
///
/// ```json
/// {
///     "username": "bob",
///     "password": "xxx_BOB_xxx"
/// }
/// ```
pub fn parse(s: &str) -> HashMap<String, String> {
    let mut m = HashMap::new();
    for unit in s.split('&') {
        let splitted: Vec<_> = unit.splitn(2, '=').collect();
        m.insert(splitted[0].to_string(), splitted[1].to_string());
    }

    m
}

/// Parse a HashMap into a cookie.
pub fn from_hashmap(m: HashMap<String, String>) -> String {
    format!(
        "email={}&uid={}&role={}",
        &m["email"], &m["uid"], &m["role"]
    )
}

/// Create a profile (email, uid, and role) for a given email.
///
/// The results of this is encoded as a structured cookie.
///
/// **Note**: This function consumes any and all ampersands `&` and equal signs `=` to stop people
/// from setting themselves as admin.
///
/// # Examples
///
/// To encode your standard email `foo@bar.com`:
///
/// ```
/// let profile = cryptopals::cookies::profile_for("foo@bar.com");
/// assert_eq!(profile, "email=foo@bar.com&uid=10&role=user");
/// ```
pub fn profile_for(email: &str) -> String {
    format!(
        "email={}&uid=10&role=user",
        email.replace("&", "").replace("=", "")
    )
}

#[cfg(test)]
mod tests {
    use super::*;

    mod test_parse_to_and_fro {
        use super::*;

        #[test]
        fn example() {
            let mut m = HashMap::new();
            m.insert("foo".to_string(), "bar".to_string());
            m.insert("baz".to_string(), "qux".to_string());
            m.insert("zap".to_string(), "zazzle".to_string());
            let s = "foo=bar&baz=qux&zap=zazzle";

            assert_eq!(parse(s), m);
        }

        #[test]
        fn inverse() {
            assert_eq!(
                from_hashmap(parse(&profile_for("mail@gmail.com"))),
                profile_for("mail@gmail.com")
            );
        }
    }

    mod test_profile_for {
        use super::*;

        #[test]
        fn example() {
            assert_eq!(
                profile_for("me@dominion.com"),
                "email=me@dominion.com&uid=10&role=user"
            );
        }

        #[test]
        fn hacker_detection() {
            assert_eq!(
                profile_for("a@b.c&role=admin"),
                "email=a@b.croleadmin&uid=10&role=user"
            );
        }
    }
}
