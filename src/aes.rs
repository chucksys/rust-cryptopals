//! A collection of functions that implement the AES standard.

use super::converts::str_to_bytes;
use super::ops::xor_bytes;

pub const BLOCK_SIZE: usize = 16;

#[derive(Debug, PartialEq)]
pub enum MODE {
    ECB,
    CBC,
}

/// Determine whether text has proper PKCS#7 padding.
///
/// Return true if it is proper padding and false otherwise.
///
/// # Examples
///
/// ```
/// use cryptopals::aes::is_valid_pkcs7;
/// use cryptopals::converts::ascii_to_bytes;
/// let mut iceice = ascii_to_bytes("ICE ICE BABY");
/// iceice.push(4);
/// iceice.push(4);
/// iceice.push(4);
/// iceice.push(4);
/// let mut bad5 = ascii_to_bytes("ICE ICE BABY");
/// bad5.push(5);
/// bad5.push(5);
/// bad5.push(5);
/// bad5.push(5);
/// let mut onetwo = ascii_to_bytes("ICE ICE BABY");
/// onetwo.push(1);
/// onetwo.push(2);
/// onetwo.push(3);
/// onetwo.push(4);
///
/// assert!(is_valid_pkcs7(&iceice, 16));
/// assert!(!is_valid_pkcs7(&bad5, 16));
/// assert!(!is_valid_pkcs7(&onetwo, 16));
/// ```
pub fn is_valid_pkcs7(bytes: &Vec<u8>, chunksize: usize) -> bool {
    assert!(!bytes.is_empty());

    let last_b = bytes[bytes.len() - 1];
    bytes.iter().rev().take(last_b as usize).for_each(|&x| println!("{}", x));
    bytes.len() % chunksize == 0 &&
        bytes.iter()
        .rev()
        .take(last_b as usize)
        .all(|&x| x == last_b)
}

/// Implementation of PKCS#7 padding.
///
/// Block ciphers usually transforms fixed-sized blocks, but if the block is irregularly-sized, we
/// need to pad it with things.
///
/// # Examples
///
/// ```
/// assert_eq!(cryptopals::aes::pad_pkcs7(vec![1, 2, 3], 8), vec![1, 2, 3, 5, 5, 5, 5, 5]);
/// ```
pub fn pad_pkcs7(bytes: Vec<u8>, block_size: usize) -> Vec<u8> {
    let mut v = bytes;
    let blocks_covered = v.len() / block_size + 1;
    let padding = blocks_covered * block_size - v.len();

    // Padding must be less than a byte
    assert!(padding < 256);
    v.append(&mut vec![padding as u8; padding]);

    v
}

/// Implementation of the unpadding function for PKCS #7 padding
pub fn unpad_pkcs7(v: Vec<u8>) -> Vec<u8> {
    let last_elem = *v.last().unwrap() as usize;

    if v.len() > last_elem && v[v.len() - last_elem..].to_vec() == vec![last_elem as u8; last_elem]
    {
        v[..v.len() - (last_elem as usize)].to_vec()
    } else {
        v
    }
}

/// Implementation of AES 128-bit encryption in ECB mode.
pub fn encrypt_ecb(text: &Vec<u8>, key: &Vec<u8>) -> Vec<u8> {
    assert_eq!(text.len() % BLOCK_SIZE, 0);

    let nr = get_nr(key.len());
    let mut ct = Vec::with_capacity(text.len());
    let w = key_expansion(key);

    for chunk in text.chunks(BLOCK_SIZE) {
        ct.append(&mut encrypt_block(chunk, &w, nr));
    }

    ct
}

/// Implementation of AES 128-bit encryption in blocks
pub fn encrypt_block(block: &[u8], w: &[Vec<u8>], nr: usize) -> Vec<u8> {
    assert_eq!(block.len(), BLOCK_SIZE);

    let mut state = block.to_vec();
    let mut i = 0;

    state = add_round_key(state, w, i);
    i += 1;

    for _ in 0..nr - 1 {
        // println!("Starting:          {}", bytes_to_str(state.clone()));
        state = sub_bytes(state);
        // println!("After SubBytes:    {}", bytes_to_str(state.clone()));
        state = shift_rows(state);
        // println!("After ShiftRows:   {}", bytes_to_str(state.clone()));
        state = mix_columns(state);
        // println!("After MixColumns:  {}", bytes_to_str(state.clone()));
        state = add_round_key(state, w, i);
        // println!("After AddRoundKey: {}", bytes_to_str(state.clone()));
        i += 1
    }

    state = sub_bytes(state);
    state = shift_rows(state);
    state = add_round_key(state, w, i);
    state
}

/// Get Nr (number of rounds) with the key size
pub fn get_nr(l: usize) -> usize {
    match l {
        16 => 10,
        24 => 12,
        32 => 14,
        _ => panic!("Unknown key size '{}'", l)
    }
}

/// Get Nr with the key size / 4
pub fn get_nk(l: usize) -> usize {
    match l {
        4 => 10,
        6 => 12,
        8 => 14,
        _ => panic!("Unknown key size / 4 '{}'", l)
    }
}

/// Implementation of AES 128-bit encryption in CBC mode
pub fn encrypt_cbc(txt: &Vec<u8>, key: &Vec<u8>, iv: &Vec<u8>) -> Vec<u8> {
    assert_eq!(txt.len() % BLOCK_SIZE, 0);

    let nr = get_nr(key.len());
    let mut ct = Vec::with_capacity(txt.len());
    let w = key_expansion(key);
    let mut last_chunk = iv.clone();
    let chunks = txt.chunks(BLOCK_SIZE);

    for chunk in chunks {
        let before = xor_bytes(chunk.to_vec(), last_chunk);
        last_chunk = encrypt_block(&before, &w, nr);
        ct.append(&mut last_chunk.clone());
    }

    ct
}

/// Implementation of AES 128-bit decryption in CBC mode
pub fn decrypt_cbc(ct: &Vec<u8>, key: &Vec<u8>, iv: &Vec<u8>) -> Vec<u8> {
    let nr = get_nr(key.len());
    let mut text = Vec::with_capacity(ct.len());
    let w = key_expansion_dec(key);
    let mut last_chunk = iv.clone();

    // We know that decrypting would always be in 16-byte blocks
    for chunk in ct.chunks(BLOCK_SIZE) {
        let before_xor = decrypt_block(chunk, &w, nr);
        text.append(&mut xor_bytes(before_xor, last_chunk));
        last_chunk = chunk.to_vec();
    }

    text
}

/// Implementation of AES 128-bit decryption in blocks
pub fn decrypt_block(block: &[u8], w: &[Vec<u8>], nr: usize) -> Vec<u8> {
    let mut state = block.to_vec();
    let mut i = 0;

    state = add_round_key(state, w, i);
    i += 1;

    for _ in 0..nr - 1 {
        // println!("Starting:          {}", bytes_to_str(state.clone()));
        state = inv_shift_rows(state);
        // println!("After ShiftRows:   {}", bytes_to_str(state.clone()));
        state = inv_sub_bytes(state);
        // println!("After SubBytes:    {}", bytes_to_str(state.clone()));
        state = add_round_key(state, w, i);
        // println!("After AddRoundKey: {}", bytes_to_str(state.clone()));
        state = inv_mix_columns(state);
        // println!("After MixColumns:  {}", bytes_to_str(state.clone()));
        i += 1;
    }

    state = inv_shift_rows(state);
    state = inv_sub_bytes(state);
    state = add_round_key(state, w, i);

    state
}

/// Implementation of AES 128-bit decryption in ECB mode.
pub fn decrypt_ecb(ct: &Vec<u8>, key: &Vec<u8>) -> Vec<u8> {
    let nr = get_nr(key.len());
    let mut text = Vec::with_capacity(ct.len());
    let w = key_expansion_dec(key);

    for chunk in ct.chunks(BLOCK_SIZE) {
        text.append(&mut decrypt_block(chunk, &w, nr));
    }

    text
}

/// Implementation of SubBytes function in accordance to AES standard.
///
/// Uses a lookup table to accomplish everything. Might be a little repetitive, since the lookup
/// table is populated by calling `converts::str_to_bytes` on a 512-byte `&'static str`, but should
/// be good enough for all intents and purposes.
pub fn sub_bytes(bytes: Vec<u8>) -> Vec<u8> {
    let table = str_to_bytes("637c777bf26b6fc53001672bfed7ab76ca82c97dfa5947f0add4a2af9ca472c0b7fd9326363ff7cc34a5e5f171d8311504c723c31896059a071280e2eb27b27509832c1a1b6e5aa0523bd6b329e32f8453d100ed20fcb15b6acbbe394a4c58cfd0efaafb434d338545f9027f503c9fa851a3408f929d38f5bcb6da2110fff3d2cd0c13ec5f974417c4a77e3d645d197360814fdc222a908846eeb814de5e0bdbe0323a0a4906245cc2d3ac629195e479e7c8376d8dd54ea96c56f4ea657aae08ba78252e1ca6b4c6e8dd741f4bbd8b8a703eb5664803f60e613557b986c11d9ee1f8981169d98e949b1e87e9ce5528df8ca1890dbfe6426841992d0fb054bb16");

    bytes.iter().map(|&b| table[b as usize]).collect()
}

/// Implementation of InvSubBytes function in accordance to AES standard.
///
/// Uses a lookup table to accomplish everything. Same with `sub_bytes`.
pub fn inv_sub_bytes(bytes: Vec<u8>) -> Vec<u8> {
    let table = str_to_bytes("52096ad53036a538bf40a39e81f3d7fb7ce339829b2fff87348e4344c4dee9cb547b9432a6c2233dee4c950b42fac34e082ea16628d924b2765ba2496d8bd12572f8f66486689816d4a45ccc5d65b6926c704850fdedb9da5e154657a78d9d8490d8ab008cbcd30af7e45805b8b34506d02c1e8fca3f0f02c1afbd0301138a6b3a9111414f67dcea97f2cfcef0b4e67396ac7422e7ad3585e2f937e81c75df6e47f11a711d29c5896fb7620eaa18be1bfc563e4bc6d279209adbc0fe78cd5af41fdda8338807c731b11210592780ec5f60517fa919b54a0d2de57a9f93c99cefa0e03b4dae2af5b0c8ebbb3c83539961172b047eba77d626e169146355210c7d");

    bytes.iter().map(|&b| table[b as usize]).collect()
}

/// Implementation of ShiftRows function in accordance to AES standard.
pub fn shift_rows(b: Vec<u8>) -> Vec<u8> {
    vec![
        b[0], b[5], b[10], b[15], b[4], b[9], b[14], b[3], b[8], b[13], b[2], b[7], b[12], b[1],
        b[6], b[11],
    ]
}

/// Implementation of InvShiftRows function in accordance to AES standard.
pub fn inv_shift_rows(b: Vec<u8>) -> Vec<u8> {
    vec![
        b[0], b[13], b[10], b[7], b[4], b[1], b[14], b[11], b[8], b[5], b[2], b[15], b[12], b[9],
        b[6], b[3],
    ]
}

/// A simple alias for multiplying 2 bytes with modulo.
///
/// This was copied from wikipedia's entry on how Rijndael's finite field works for multiplication
/// over the field. We use the Russian Peasant Multiplication Algorithm here.
///
/// Note: **This function should not be used in systems of actual importance because of timing
/// difference issues, caches, and all the like.**
fn mul(a: u8, b: u8) -> u8 {
    let mut p = 0;
    let mut a = a;
    let mut b = b;

    while a > 0 && b > 0 {
        if b & 1 == 1 {
            // b is odd
            p ^= a;
        }

        if a & 0x80 == 0x80 {
            // a must be reduced if a >= 128
            let over_a: u16 = (a as u16) << 1;
            a = (over_a ^ 0x11b) as u8;
        } else {
            a <<= 1;
        }

        b >>= 1;
    }

    p
}

/// Implementation of MixColumns function in accordance to AES standard.
pub fn mix_columns(b: Vec<u8>) -> Vec<u8> {
    let mut v = b.clone();

    for c in 0..4 {
        v[c * 4] = mul(2, b[c * 4]) ^ mul(3, b[c * 4 + 1]) ^ b[c * 4 + 2] ^ b[c * 4 + 3];
        v[c * 4 + 1] = b[c * 4] ^ mul(2, b[c * 4 + 1]) ^ mul(3, b[c * 4 + 2]) ^ b[c * 4 + 3];
        v[c * 4 + 2] = b[c * 4] ^ b[c * 4 + 1] ^ mul(2, b[c * 4 + 2]) ^ mul(3, b[c * 4 + 3]);
        v[c * 4 + 3] = mul(3, b[c * 4]) ^ b[c * 4 + 1] ^ b[c * 4 + 2] ^ mul(2, b[c * 4 + 3]);
    }

    v
}

/// Implementation of InvMixColumns function in accordance to AES standard.
pub fn inv_mix_columns(b: Vec<u8>) -> Vec<u8> {
    let mut v = b.clone();

    for c in 0..4 {
        v[c * 4] = mul(0xe, b[c * 4])
            ^ mul(0xb, b[c * 4 + 1])
            ^ mul(0xd, b[c * 4 + 2])
            ^ mul(9, b[c * 4 + 3]);
        v[c * 4 + 1] = mul(9, b[c * 4])
            ^ mul(0xe, b[c * 4 + 1])
            ^ mul(0xb, b[c * 4 + 2])
            ^ mul(0xd, b[c * 4 + 3]);
        v[c * 4 + 2] = mul(0xd, b[c * 4])
            ^ mul(9, b[c * 4 + 1])
            ^ mul(0xe, b[c * 4 + 2])
            ^ mul(0xb, b[c * 4 + 3]);
        v[c * 4 + 3] = mul(0xb, b[c * 4])
            ^ mul(0xd, b[c * 4 + 1])
            ^ mul(9, b[c * 4 + 2])
            ^ mul(0xe, b[c * 4 + 3]);
    }

    v
}

/// Implementation of AddRoundKey function in accordance to AES standard.
pub fn add_round_key(b: Vec<u8>, w: &[Vec<u8>], ind: usize) -> Vec<u8> {
    let mut v = b.clone();
    let ind = ind * 4;

    for c in 0..4 {
        v[c * 4] = b[c * 4] ^ w[ind + c][0];
        v[c * 4 + 1] = b[c * 4 + 1] ^ w[ind + c][1];
        v[c * 4 + 2] = b[c * 4 + 2] ^ w[ind + c][2];
        v[c * 4 + 3] = b[c * 4 + 3] ^ w[ind + c][3];
    }

    v
}

/// Implementation of the weird Rcon in accordance to AES standard.
pub fn rcon(i: u32) -> Vec<u8> {
    let weirds = vec![1, 2, 4, 8, 0x10, 0x20, 0x40, 0x80, 0x1b, 0x36];
    vec![weirds[(i - 1) as usize], 0, 0, 0]
}

/// Implementation of RotWord in accordance to AES standard.
pub fn rot_word(b: Vec<u8>) -> Vec<u8> {
    vec![b[1], b[2], b[3], b[0]]
}

/// Implementation of KeyExpansion function in accordance to AES standard.
///
/// Supports key sizes 128, 192, 256 bits.
pub fn key_expansion(key: &Vec<u8>) -> Vec<Vec<u8>> {
    let nk = key.len() / 4;
    let nr = get_nk(nk);
    let mut w = Vec::with_capacity(4 * (nr + 1));
    let mut i = 0;

    // Push the key into first word slots
    while i < nk {
        w.push(key[4 * i..4 * i + 4].to_vec());
        i += 1;
    }

    // Use algorithm for the rest
    while i < 4 * (nr + 1) {
        let mut temp = w[i - 1].clone();
        if i % nk == 0 {
            temp = xor_bytes(sub_bytes(rot_word(temp)), rcon((i / nk) as u32));
        } else if nk > 6 && i % nk == 4 {
            temp = sub_bytes(temp);
        }

        w.push(xor_bytes(w[i - nk].clone(), temp));
        i += 1;
    }

    w
}

/// Implementation of modified KeyExpansion function in accordance to AES standard, to suit
/// decryption.
///
/// Supports key sizes 128, 192, 256 bits.
pub fn key_expansion_dec(key: &Vec<u8>) -> Vec<Vec<u8>> {
    let nk = key.len() / 4;
    let nr = get_nk(nk);
    let mut w = Vec::with_capacity(4 * (nr + 1));
    let mut i = 0;

    // Push the key into first word slots
    while i < nk {
        w.push(key[4 * i..4 * i + 4].to_vec());
        i += 1;
    }

    // Use algorithm for the rest
    while i < 4 * (nr + 1) {
        let mut temp = w[i - 1].clone();
        if i % nk == 0 {
            temp = xor_bytes(sub_bytes(rot_word(temp)), rcon((i / nk) as u32));
        } else if nk > 6 && i % nk == 4 {
            temp = sub_bytes(temp);
        }

        w.push(xor_bytes(w[i - nk].clone(), temp));
        i += 1;
    }

    // We now reverse for every 16 bytes (which is a block)
    // We do so by inserting in reverse order
    let mut dw = Vec::new();
    for (i, ch) in w.into_iter().enumerate() {
        dw.insert(i % 4, ch);
    }

    dw
}

#[cfg(test)]
mod tests {
    use super::*;

    mod test_cbc {
        use super::{decrypt_cbc, encrypt_cbc, pad_pkcs7, unpad_pkcs7};

        #[test]
        fn inverse() {
            let txt = pad_pkcs7(b"hello, everyone! I am happy!!! Happy to have fun!!1".to_vec(), 16);
            let key = b"yellow submarine".to_vec();
            let iv = b"abcarstqwf432198".to_vec();
            let ct = encrypt_cbc(&txt, &key, &iv);
            let decrypted = decrypt_cbc(&ct, &key, &iv);

            assert_eq!(txt, decrypted);
        }
    }

    mod test_pkcs7_padding {
        use super::{pad_pkcs7, unpad_pkcs7};

        #[test]
        fn small() {
            assert_eq!(pad_pkcs7(vec![1, 2, 3, 4], 8), vec![1, 2, 3, 4, 4, 4, 4, 4]);
            assert_eq!(unpad_pkcs7(vec![1, 2, 3, 4, 4, 4, 4]), vec![1, 2, 3]);
        }

        #[test]
        fn edge_cases() {
            assert_eq!(unpad_pkcs7(vec![1, 2, 3, 4, 4, 4, 4, 4]), vec![1, 2, 3, 4]);
        }

        #[test]
        fn inverse() {
            assert_eq!(
                unpad_pkcs7(pad_pkcs7(vec![1, 2, 3, 4], 20)),
                vec![1, 2, 3, 4]
            );
        }

        #[test]
        fn all_changes_necessary() {
            assert_eq!(unpad_pkcs7(vec![1, 2, 3, 4, 4, 4, 4, 4]), vec![1, 2, 3, 4]);
            assert_eq!(pad_pkcs7(vec![1, 2, 3, 4], 4), vec![1, 2, 3, 4, 4, 4, 4, 4]);
        }
    }

    mod test_encrypt_ecb {
        use super::encrypt_ecb;
        use super::str_to_bytes;

        #[test]
        fn example_on_fips197() {
            let inp = str_to_bytes("3243f6a8885a308d313198a2e0370734");
            let key = str_to_bytes("2b7e151628aed2a6abf7158809cf4f3c");
            let ct = encrypt_ecb(&inp, &key);
            let expect = str_to_bytes("3925841d02dc09fbdc118597196a0b32");

            assert_eq!(ct, expect);
        }
    }

    mod test_decrypt_ecb {
        use super::str_to_bytes;
        use super::{decrypt_ecb, encrypt_ecb};

        #[test]
        fn example_on_fips197() {
            let inp = str_to_bytes("69c4e0d86a7b0430d8cdb78070b4c55a");
            let key = str_to_bytes("000102030405060708090a0b0c0d0e0f");
            let txt = decrypt_ecb(&inp, &key);
            let expect = str_to_bytes("00112233445566778899aabbccddeeff");

            assert_eq!(txt, expect);
        }

        #[test]
        fn inverse() {
            let inp = b"Hello, world!!!!".to_vec();
            let key = b"Yellow subMarine".to_vec();

            assert_eq!(decrypt_ecb(&encrypt_ecb(&inp, &key), &key), inp);
        }
    }

    mod test_sub_bytes {
        use super::sub_bytes;

        #[test]
        fn example_on_fips197() {
            assert_eq!(sub_bytes(vec![0x53]), vec![0xed]);
        }
    }

    mod test_inv_sub_bytes {
        use super::{inv_sub_bytes, sub_bytes};

        #[test]
        fn inverse() {
            let v = vec![1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16];
            assert_eq!(inv_sub_bytes(sub_bytes(v.clone())), v);
            assert_eq!(sub_bytes(inv_sub_bytes(v.clone())), v);
        }
    }

    mod test_shift_rows {
        use super::shift_rows;

        #[test]
        fn example_on_fips197() {
            assert_eq!(
                shift_rows(vec![1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16]),
                vec![1, 6, 11, 16, 5, 10, 15, 4, 9, 14, 3, 8, 13, 2, 7, 12]
            );
        }
    }

    mod test_inv_shift_rows {
        use super::{inv_shift_rows, shift_rows};

        #[test]
        fn inverse() {
            let v = vec![1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16];
            assert_eq!(inv_shift_rows(shift_rows(v.clone())), v);
            assert_eq!(shift_rows(inv_shift_rows(v.clone())), v);
        }
    }

    mod test_mix_columns {
        use super::mix_columns;
        use super::str_to_bytes;

        #[test]
        fn example_on_fips197() {
            assert_eq!(
                mix_columns(str_to_bytes("d4bf5d30e0b452aeb84111f11e2798e5")),
                str_to_bytes("046681e5e0cb199a48f8d37a2806264c")
            );
        }
    }

    mod test_inv_mix_columns {
        use super::{inv_mix_columns, mix_columns};

        #[test]
        fn inverse() {
            let v = vec![1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16];
            assert_eq!(inv_mix_columns(mix_columns(v.clone())), v);
            assert_eq!(mix_columns(inv_mix_columns(v.clone())), v);
        }
    }

    mod test_key_expansion_dec {
        use super::key_expansion_dec;
        use super::str_to_bytes;

        #[test]
        fn small_test() {
            let key = str_to_bytes("000102030405060708090a0b0c0d0e0f");
            let expanded = key_expansion_dec(&key);

            assert_eq!(expanded[0], str_to_bytes("13111d7f"));
        }
    }

    mod test_key_expansion {
        use super::key_expansion;
        use super::str_to_bytes;

        #[test]
        fn example_on_fips_nk_4() {
            let key = str_to_bytes("2b7e151628aed2a6abf7158809cf4f3c");
            let expanded = key_expansion(&key);
            let expected = vec![
                str_to_bytes("2b7e1516"),
                str_to_bytes("28aed2a6"),
                str_to_bytes("abf71588"),
                str_to_bytes("09cf4f3c"),
                str_to_bytes("a0fafe17"),
                str_to_bytes("88542cb1"),
                str_to_bytes("23a33939"),
                str_to_bytes("2a6c7605"),
                str_to_bytes("f2c295f2"),
                str_to_bytes("7a96b943"),
                str_to_bytes("5935807a"),
                str_to_bytes("7359f67f"),
                str_to_bytes("3d80477d"),
                str_to_bytes("4716fe3e"),
                str_to_bytes("1e237e44"),
                str_to_bytes("6d7a883b"),
                str_to_bytes("ef44a541"),
                str_to_bytes("a8525b7f"),
                str_to_bytes("b671253b"),
                str_to_bytes("db0bad00"),
                str_to_bytes("d4d1c6f8"),
                str_to_bytes("7c839d87"),
                str_to_bytes("caf2b8bc"),
                str_to_bytes("11f915bc"),
                str_to_bytes("6d88a37a"),
                str_to_bytes("110b3efd"),
                str_to_bytes("dbf98641"),
                str_to_bytes("ca0093fd"),
                str_to_bytes("4e54f70e"),
                str_to_bytes("5f5fc9f3"),
                str_to_bytes("84a64fb2"),
                str_to_bytes("4ea6dc4f"),
                str_to_bytes("ead27321"),
                str_to_bytes("b58dbad2"),
                str_to_bytes("312bf560"),
                str_to_bytes("7f8d292f"),
                str_to_bytes("ac7766f3"),
                str_to_bytes("19fadc21"),
                str_to_bytes("28d12941"),
                str_to_bytes("575c006e"),
                str_to_bytes("d014f9a8"),
                str_to_bytes("c9ee2589"),
                str_to_bytes("e13f0cc8"),
                str_to_bytes("b6630ca6"),
            ];

            assert_eq!(expanded, expected);
        }

        #[test]
        fn example_on_fips_nk_6() {
            let key = str_to_bytes("8e73b0f7da0e6452c810f32b809079e562f8ead2522c6b7b");
            let expanded = key_expansion(&key);
            let expected = vec![
                str_to_bytes("8e73b0f7"),
                str_to_bytes("da0e6452"),
                str_to_bytes("c810f32b"),
                str_to_bytes("809079e5"),
                str_to_bytes("62f8ead2"),
                str_to_bytes("522c6b7b"),
                str_to_bytes("fe0c91f7"),
                str_to_bytes("2402f5a5"),
                str_to_bytes("ec12068e"),
                str_to_bytes("6c827f6b"),
                str_to_bytes("0e7a95b9"),
                str_to_bytes("5c56fec2"),
                str_to_bytes("4db7b4bd"),
                str_to_bytes("69b54118"),
                str_to_bytes("85a74796"),
                str_to_bytes("e92538fd"),
                str_to_bytes("e75fad44"),
                str_to_bytes("bb095386"),
                str_to_bytes("485af057"),
                str_to_bytes("21efb14f"),
                str_to_bytes("a448f6d9"),
                str_to_bytes("4d6dce24"),
                str_to_bytes("aa326360"),
                str_to_bytes("113b30e6"),
                str_to_bytes("a25e7ed5"),
                str_to_bytes("83b1cf9a"),
                str_to_bytes("27f93943"),
                str_to_bytes("6a94f767"),
                str_to_bytes("c0a69407"),
                str_to_bytes("d19da4e1"),
                str_to_bytes("ec1786eb"),
                str_to_bytes("6fa64971"),
                str_to_bytes("485f7032"),
                str_to_bytes("22cb8755"),
                str_to_bytes("e26d1352"),
                str_to_bytes("33f0b7b3"),
                str_to_bytes("40beeb28"),
                str_to_bytes("2f18a259"),
                str_to_bytes("6747d26b"),
                str_to_bytes("458c553e"),
                str_to_bytes("a7e1466c"),
                str_to_bytes("9411f1df"),
                str_to_bytes("821f750a"),
                str_to_bytes("ad07d753"),
                str_to_bytes("ca400538"),
                str_to_bytes("8fcc5006"),
                str_to_bytes("282d166a"),
                str_to_bytes("bc3ce7b5"),
                str_to_bytes("e98ba06f"),
                str_to_bytes("448c773c"),
                str_to_bytes("8ecc7204"),
                str_to_bytes("01002202"),
            ];

            assert_eq!(expanded, expected);
        }
    }
}
