//! A collection of functions that try to break some ciphers.

extern crate rand;

use super::aes;
use super::ops::xor_bytes;
use rand::prelude::*;
use std::collections::HashSet;

/// Decrypt hex string with repeating 1 key using English character frequencies.
///
/// Checks to see if the string is valid by checking the amount of valid ascii characters there
/// are.
pub fn dec_hex_rep1(hex: impl IntoIterator<Item = u8> + Clone) -> u8 {
    (0..255)
        .max_by_key(|key| {
            hex.clone().into_iter().fold(0, |acc, x| {
                let xored = x ^ key;
                if xored == b'e'
                    || xored == b't'
                    || xored == b'a'
                    || xored == b'o'
                    || xored == b'i'
                    || xored == b' '
                {
                    acc + 2
                } else if xored.is_ascii_alphabetic() {
                    acc + 1
                } else if !xored.is_ascii() {
                    acc - 1
                } else {
                    acc
                }
            })
        })
        .unwrap()
}

/// A repeating key xor implementation.
pub fn rep_key_xor<T: IntoIterator<Item = u8>>(hex: T, key: T) -> Vec<u8>
where
    <T as IntoIterator>::IntoIter: Clone,
{
    xor_bytes(hex, key.into_iter().cycle())
}

/// Find the number of blocks that are repeating.
pub fn count_repeating_blocks(bs: Vec<u8>, block_size: usize) -> usize {
    let mut set = HashSet::new();
    let mut num_chunks = 0;
    bs.chunks(block_size).for_each(|chunk| {
        set.insert(chunk);
        num_chunks += 1;
    });

    num_chunks - set.len()
}

/// Encrypts data using a random key and random IV (if needed), using AES ECB and AES CBC 50% of
/// the time each.
///
/// This function appends 5-10 bytes before the data and after the data, before going through with
/// encryption. It outputs the cipher text and the scheme used to encrypt (for easy checking).
pub fn encryption_oracle(bs: Vec<u8>) -> (Vec<u8>, aes::MODE) {
    let mode = if random() {
        aes::MODE::ECB
    } else {
        aes::MODE::CBC
    };
    let key = random_bytes(aes::BLOCK_SIZE);
    let front_bytes = rand::thread_rng().gen_range(5, 11);
    let back_bytes = rand::thread_rng().gen_range(5, 11);

    // Push the random bytes to the front and back of the array
    let mut bs = bs;
    for _ in 0..front_bytes {
        bs.insert(0, random());
    }
    for _ in 0..back_bytes {
        bs.push(random());
    }

    let bs = aes::pad_pkcs7(bs, 16);

    match mode {
        aes::MODE::ECB => (aes::encrypt_ecb(&bs, &key), mode),
        aes::MODE::CBC => {
            let iv = random_bytes(aes::BLOCK_SIZE);
            (aes::encrypt_cbc(&bs, &key, &iv), mode)
        }
    }
}

/// Guesses the encryption mode of operation (AES CBC or ECB) used.
pub fn guess_aes_moo(bs: Vec<u8>) -> aes::MODE {
    if count_repeating_blocks(bs, aes::BLOCK_SIZE) > 0 {
        aes::MODE::ECB
    } else {
        aes::MODE::CBC
    }
}

/// Function that creates a function that encrypts using AES ECB mode by first appending a constant
/// plaintext, encrypting with the provided key.
pub fn make_oracle(append: usize, pt: Vec<u8>, key: Vec<u8>) -> impl Fn(Vec<u8>) -> Vec<u8> {
    // We use move because we want to force the closure to take ownership of both pt and key
    let rands = random_bytes(append);
    move |text: Vec<u8>| {
        let mut to_encrypt = rands.clone();
        to_encrypt.append(&mut text.clone());
        to_encrypt.append(&mut pt.clone());
        let to_encrypt = aes::pad_pkcs7(to_encrypt, 16);
        aes::encrypt_ecb(&to_encrypt, &key)
    }
}

/// Find the block size, given an oracle.
pub fn get_block_size(oracle: impl Fn(Vec<u8>) -> Vec<u8>) -> usize {
    // We add letters until we get to another block
    let starting_len = oracle(Vec::new()).len();
    for size in 1.. {
        let bytes = vec![b'A'; size];
        let new_len = oracle(bytes).len();

        if new_len > starting_len {
            return new_len - starting_len;
        }
    }

    panic!(
        "Could not find block size; how did you get here? It should have been an infinite loop!"
    );
}

/// Find the length of the random text that is prepended to the plaintext before encryption in an
/// oracle.
pub fn get_randbytes_len(oracle: impl Fn(Vec<u8>) -> Vec<u8>, block_size: usize) -> usize {
    let no_mod_ct = oracle(Vec::new());
    let single_char_ct = oracle(vec![0]);
    let mut block = 0;

    // We first find which block the random text ends
    let num_blocks = no_mod_ct.len() / block_size;
    for i in 0..num_blocks {
        if &no_mod_ct[i * block_size..(i + 1) * block_size]
            == &single_char_ct[i * block_size..(i + 1) * block_size]
        {
            block += 1;
        } else {
            break;
        }
    }

    // After we obtain the block that the random text ends, we systematically add bytes and check
    // to see when the block we are watching stays the same (which means the amount of bytes we
    // added has just left the block) and add both of them together.
    let target_block = &oracle(vec![0; block_size])[block * block_size..(block + 1) * block_size];
    for size in (0..block_size + 1).rev() {
        let bytes = vec![0; size];
        let new_block = &oracle(bytes)[block * block_size..(block + 1) * block_size];

        if target_block != new_block {
            return block * block_size + (block_size - size - 1);
        }
    }

    panic!("Code should not reach this point");
}

/// Find the plain text that is appended to your plain text before encryption, within your oracle.
pub fn get_oracle_plaintext(oracle: impl Fn(Vec<u8>) -> Vec<u8>, block_size: usize) -> Vec<u8> {
    let mut pt = Vec::new();
    let text_len = oracle(Vec::new()).len();
    let num_blocks = text_len / block_size;
    let mut block = vec![0; block_size - 1];

    for i in 0..num_blocks {
        for b_pos in 0..block_size {
            // Find the value of the block with 1 character unknown
            let to_find = oracle(vec![0; block_size - b_pos - 1])
                [i * block_size..(i + 1) * block_size]
                .to_vec();
            // Iterate through oracle where we only change the last character
            let mut added = false;
            for t in 0..256 {
                let mut attempt = block.clone();
                attempt.push(t as u8);
                let found = oracle(attempt)[0..block_size].to_vec();
                if to_find == found {
                    // Push to tail and remove from the head
                    block.push(t as u8);
                    block.remove(0);
                    // Push to returning vector
                    pt.push(t as u8);
                    added = true;

                    break;
                }
            }

            if !added {
                return pt;
            }
        }
    }

    pt
}

/// Function that generates a random block of bytes.
pub fn random_bytes(size: usize) -> Vec<u8> {
    let mut bytes = vec![0; size];
    rand::thread_rng().fill_bytes(&mut bytes);
    bytes
}

#[cfg(test)]
mod tests {
    use super::*;

    mod test_get_randbytes_len {
        use super::*;

        #[test]
        fn zero() {
            let oracle = make_oracle(0, random_bytes(14), random_bytes(16));
            assert_eq!(get_randbytes_len(oracle, 16), 0);
        }

        #[test]
        fn one_block() {
            let oracle = make_oracle(7, random_bytes(14), random_bytes(16));
            assert_eq!(get_randbytes_len(oracle, 16), 7);
        }

        #[test]
        fn multi_block() {
            let oracle = make_oracle(47, random_bytes(14), random_bytes(16));
            assert_eq!(get_randbytes_len(oracle, 16), 47);
        }

        #[test]
        fn multi_block_hit_boundary() {
            let oracle = make_oracle(48, random_bytes(14), random_bytes(16));
            assert_eq!(get_randbytes_len(oracle, 16), 48);
        }

        #[test]
        fn multi_block_pt_and_rands() {
            let oracle = make_oracle(48, random_bytes(25), random_bytes(16));
            assert_eq!(get_randbytes_len(oracle, 16), 48);
        }

        #[test]
        fn single_rands_and_multi_pt() {
            let oracle = make_oracle(5, random_bytes(25), random_bytes(16));
            assert_eq!(get_randbytes_len(oracle, 16), 5);
        }
    }

    mod test_random_bytes {
        use super::*;

        #[test]
        fn check_len() {
            assert_eq!(random_bytes(256).len(), 256);
        }
    }

    mod test_rep_key_xor {
        use super::*;

        #[test]
        fn standard() {
            assert_eq!(rep_key_xor(vec![0], vec![1]), vec![1]);
            assert_eq!(rep_key_xor(vec![0], vec![0]), vec![0]);
        }

        #[test]
        fn different_sizes() {
            assert_eq!(rep_key_xor(vec![0; 10], vec![2; 2]), vec![2; 10]);
            assert_eq!(rep_key_xor(vec![1; 10], vec![3; 3]), vec![2; 10]);
        }
    }

    mod test_count_repeating_blocks {
        use super::*;

        #[test]
        fn standard() {
            assert_eq!(count_repeating_blocks(vec![1, 2, 3, 4, 1, 2, 3, 4], 4), 1);
            assert_eq!(count_repeating_blocks(vec![1, 2, 3, 4, 1, 2, 3, 4], 2), 2);
            assert_eq!(count_repeating_blocks(vec![1, 2, 3, 4, 1], 3), 0);
        }
    }
}
